import { render, screen, fireEvent } from "@testing-library/react";
import { ItemCard } from "./ItemCard.component";


describe("ItemCard", () => {
  
  it("should call openModalHandler function on 'Add to cart' button click", 
    () => {
      const mockedOpenModalHandler = jest.fn();
      render(<ItemCard 
        item={{
          articul: 10
        }}
        openModalHandler={mockedOpenModalHandler}
      />);
      fireEvent.click(screen.getByText("Add to cart"));
      expect(mockedOpenModalHandler).toHaveBeenCalled();
    }
  )

  it("should call stageAddCartItemArtHandler function with item.articul", 
  () => {
    const mockedStageAddCartItemArtHandler = jest.fn();
    render(<ItemCard 
      stageAddCartItemArtHandler={mockedStageAddCartItemArtHandler} 
      item={{
        articul: 10
      }}
    />);
    fireEvent.click(screen.getByText("Add to cart"));
    expect(mockedStageAddCartItemArtHandler).toHaveBeenCalledWith(10);
  }
)
})
import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import {itemsReducer} from "./items/items-reducer";
import {modalReducer} from "./modal/modal-reducer";
import { composeWithDevTools } from "redux-devtools-extension";

const rootReducer = combineReducers({
    itemsData: itemsReducer, 
    modalData: modalReducer,
})

const middlewareEnhancer = applyMiddleware(thunk)
const composedEnhancers = composeWithDevTools(middlewareEnhancer)

const store = createStore(rootReducer, composedEnhancers);

window.store = store;

export default store;
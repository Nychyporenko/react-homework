import React from "react";
import { Modal } from "./components/Modal/Modal.component";
import { ItemsList } from "./components/ItemsList/ItemsList.component";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isModalOpen: false,
      items: null,
      favItemsArts: [],
      cartItemsArts: [],
      addToCartArt: null,
    }
  }

  componentDidMount() {
    fetch("./shop-data.json")
    .then(res => {
      if(res.ok) {
        return res.json()
      }
      return new Error("Failed to load items")
    })
    .then(items => {
      this.setState(items);
    })
    .catch((e) => {
      console.log(e.message);
    })

    const favItemsArts = localStorage.getItem("favItemsArts")
    ? JSON.parse(localStorage.getItem("favItemsArts"))
    : [];
    this.setState({favItemsArts})

    const cartItemsArts = localStorage.getItem("cartItemsArts")
    ? JSON.parse(localStorage.getItem("cartItemsArts"))
    : [];
    this.setState({cartItemsArts})
  }


  modalOpenHandler = () => {
    this.setState({isModalOpen: true})
  }


  modalCloseHandler = () => {
    this.setState({isModalOpen: false})
  }


  changeFavItemsHandler = (itemArticul) => {
    const {favItemsArts} = this.state;
    const isFav = favItemsArts.includes(itemArticul);
    const newFavItemsArts = isFav
    ? favItemsArts.filter(articul => articul !== itemArticul)
    : [...favItemsArts , itemArticul];
    localStorage.setItem("favItemsArts", JSON.stringify(newFavItemsArts));    
    this.setState({favItemsArts: newFavItemsArts})
  }

  changeCartItemsHandler = () => {
    const {cartItemsArts, addToCartArt} = this.state;
    const isInCart = cartItemsArts.includes(cartItemsArts);
    const newCartItemsArts = isInCart
    ? cartItemsArts.filter(articul => articul !== addToCartArt)
    : [...cartItemsArts , addToCartArt];
    localStorage.setItem("cartItemsArts", JSON.stringify(newCartItemsArts));    
    this.setState({cartItemsArts: newCartItemsArts})
  }

  addToCartArtHandler = (itemArticul) => {
    this.setState({addToCartArt: itemArticul});    
  }


  render() {
    const {items, isModalOpen, favItemsArts, cartItemsArts} = this.state;
    return (<>
      {
        !Array.isArray(items) &&
        <div>No items found</div>
      }
      <ItemsList 
        items={items}
        cartItemsArts={cartItemsArts}
        modalOpenHandler={this.modalOpenHandler}
        favItemsArts={favItemsArts}
        changeFavItemsHandler={this.changeFavItemsHandler}
        addToCartArtHandler={this.addToCartArtHandler}
      />
      {
        isModalOpen &&
        <Modal
          modalCloseHandler={this.modalCloseHandler}
          changeCartItemsHandler={this.changeCartItemsHandler}
        />
      }
    </>)
  }
}

export default App
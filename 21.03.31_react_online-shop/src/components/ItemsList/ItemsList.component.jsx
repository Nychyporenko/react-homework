import React from "react";
import { ItemCard } from "../ItemCard/ItemCard.component";
import styles from "./ItemsList.module.scss";
import PropTypes from 'prop-types';

class ItemsList extends React.Component {

    render() {

        const {items, modalOpenHandler, favItemsArts, changeFavItemsHandler, addToCartArtHandler, cartItemsArts} = this.props;
        return(
            <ul className={styles["items-list"]}>
                {items && 
                    items.map(item => {
                        const qtyInCart = cartItemsArts.filter(art => art === item.articul).length;
                        return( 
                            <ItemCard 
                                key={item.articul}
                                item={item}
                                modalOpenHandler={modalOpenHandler}
                                changeFavItemsHandler={changeFavItemsHandler}
                                isFav={favItemsArts.includes(item.articul)}
                                addToCartArtHandler={addToCartArtHandler}
                                qtyInCart={qtyInCart}
                            />
                        )   
                    })                    
                }
            </ul>
        )
    }
}

ItemsList.propTypes = {
    items: PropTypes.number, 
    modalOpenHandler: PropTypes.func, 
    favItemsArts: PropTypes.array, 
    changeFavItemsHandler: PropTypes.func, 
    addToCartArtHandler: PropTypes.func, 
    cartItemsArts: PropTypes.array,
};

ItemsList.defaultProps = {
    items: [],
    favItemsArts: [], 
    cartItemsArts: [],
}

export {ItemsList};
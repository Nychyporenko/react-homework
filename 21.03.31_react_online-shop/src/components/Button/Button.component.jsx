import React from "react";
import styles from "./button.module.scss";

class Button extends React.Component {
    render() {
      console.log(styles)
      const {text, onClick} = this.props;
      return (
        <button
          onClick={onClick}
        >{text}</button>
      )
    }
  }

  export {Button};
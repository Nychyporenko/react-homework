import React from "react";
import styles from "./ItemCard.module.scss";
import PropTypes from 'prop-types';

class ItemCard extends React.Component {

    render() {
        const {item, modalOpenHandler, isFav, changeFavItemsHandler, addToCartArtHandler, qtyInCart} = this.props;
        return (<>
            {item && 
                <li 
                    className={styles["item-card"]}
                >
                    <img 
                        className={styles["item-img"]}
                        src={item.path}
                        alt=""
                        width="100px" height="120px"
                    />
                    <img
                        className={styles["item-heart"]} 
                        src={isFav ? "./icons/heart-liked.svg" : "./icons/heart-empty.svg"}
                        height="20" width="20" alt="heart"
                        onClick={() => {changeFavItemsHandler(item.articul)}}
                    />
                    <div className={styles["item-description"]}>
                        <h2 className={styles["item-title"]}>{item.name}</h2>
                        <div className={styles["item-price-row"]}>
                            <span className={styles["item-price"]}>{`$${item.price}`}</span>
                            <button 
                                className={styles["add-to-cart-btn"]}
                                onClick={() => {
                                    modalOpenHandler()
                                    addToCartArtHandler(item.articul)
                                }}
                            >Add to cart</button>
                            {
                                qtyInCart > 0 && <span className={styles["qty-in-cart"]}>{qtyInCart}</span>
                            }
                        </div>
                    </div>
                </li>
            }
        </>)
    }
}

ItemCard.propTypes = {
    item: PropTypes.string, 
    modalOpenHandler: PropTypes.func, 
    isFav: PropTypes.bool,
    changeFavItemsHandler: PropTypes.func, 
    addToCartArtHandler: PropTypes.func, 
    qtyInCart: PropTypes.number,
};

export {ItemCard};
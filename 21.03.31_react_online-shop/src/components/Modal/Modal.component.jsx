import React from "react";
import styles from "./modal.module.scss";
import PropTypes from 'prop-types';

class Modal extends React.Component {

  render() {

    const {modalCloseHandler, changeCartItemsHandler} = this.props;
    return (
    <div
      className={styles["backdrop"]}      
      onClick={modalCloseHandler}
    >
      <div className={styles.modal}
          onClick={e => {e.stopPropagation()}}
      >        
        <div className={styles["modal-header"]}>
          <span>Add item to cart?</span>
          <button
            className={styles.closeModalBtn}
            onClick={modalCloseHandler}
          >✕</button>
        </div>
        <div className={styles["modal-actions"]}>
          <button onClick={
            () => {
              changeCartItemsHandler();
              modalCloseHandler();
            }
          }>ok</button>
          <button onClick={modalCloseHandler}>cancel</button>
        </div>
      </div>
    </div>)
  }
}

Modal.propTypes = {
  modalCloseHandler: PropTypes.func,
  changeCartItemsHandler: PropTypes.func,
};

export {Modal}
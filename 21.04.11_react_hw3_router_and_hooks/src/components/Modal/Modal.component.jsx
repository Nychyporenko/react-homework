import React from "react";
import styles from "./modal.module.scss";

const Modal = (props) => {
  console.log(props)
  const {modalProps: {setAddtionModalState, onApprove, text}} = props;
  console.log(setAddtionModalState)
  return (
  <div
    className={styles["backdrop"]}      
    onClick={() => {setAddtionModalState(false)}}
  >
    <div className={styles.modal}
        onClick={e => {e.stopPropagation()}}
    >        
      <div className={styles["modal-header"]}>
        <span>{text}</span>
        <button
          className={styles.closeModalBtn}
          onClick={() => {setAddtionModalState(false)}}
        >✕</button>
      </div>
      <div className={styles["modal-actions"]}>
        <button onClick={() => {
            onApprove();
            setAddtionModalState(false);
        }}>ok</button>
        <button onClick={() => {
            setAddtionModalState(false)
        }}>cancel</button>
      </div>
    </div>
  </div>)
}

export {Modal}
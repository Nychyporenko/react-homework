import React from "react";
import styles from "./ItemsList.module.scss";
import { Modal } from "../../components/Modal/Modal.component";

const ItemsList = (props) => {

    const {
        isLoadingSuccess,
        items,
        Card,
        itemsListProps: {
            cardProps,
            isAdditionModalOpen,
            modalProps,
            cartItemsArts,
            favItemsArts,
        },
    } = props;

    return(<>
        <ul className={styles["items-list"]}>
            {isLoadingSuccess && 
                items.map(item => {
                    const qtyInCart = cartItemsArts.filter(art => art === item.articul).length;
                    const isFav = favItemsArts.includes(item.articul);
                    const isLast = cartItemsArts.reduce(
                        (occurences, articul) => {return occurences + (articul === item.articul ? 1 : 0)},
                        0
                    ) == 1
                    ? true
                    : false
                    return( 
                        <Card 
                            key={item.articul}
                            item={item}
                            cardProps={cardProps}
                            qtyInCart={qtyInCart}
                            isFav={isFav}
                            isLast={isLast}
                        />
                    )   
                })                    
            }
        </ul>
        {
            isAdditionModalOpen &&
            <Modal
                modalProps={modalProps}
            />
        }        
    </>)
}

export {ItemsList};
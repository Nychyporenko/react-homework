import React, {useEffect, useState} from "react";
import {
  Switch,
  Route,
  BrowserRouter,
} from "react-router-dom";

import { MainPage } from "./pages/main-page/MainPage.component";
import { CartPage } from "./pages/cart-page/CartPage.component";
import { FavouritesPage } from "./pages/favourites-page/FavouritesPage.component";
import { Header } from "./components/Header/Header.component";

const App = () => {

  const [isAdditionModalOpen, setAddtionModalState] = useState(false);
  const [items, setItems] = useState(null);
  const [favItemsArts, setFavItemsArts] = useState([]);
  const [cartItemsArts, setCartItemsArts ] = useState([]);
  const [addToCartArt, setAddToCartArt] = useState(null);
  const [deleteFromCartArt, setDeleteFromCartArt] = useState(null);
  const [isLoadingSuccess, setIsLoadingSuccess] = useState(false);

  useEffect(() => {
    fetch("./shop-data.json")
    .then(res => {
      if(res.ok) {
        return res.json()
      }
      return new Error("Failed to load items")
    })
    .then(({items}) => {
      setItems(items);
      setIsLoadingSuccess(true);
    })
    .catch((e) => {
      console.log(e.message);
    })

    const favItemsArts = localStorage.getItem("favItemsArts")
    ? JSON.parse(localStorage.getItem("favItemsArts"))
    : [];
    setFavItemsArts(favItemsArts)

    const cartItemsArts = localStorage.getItem("cartItemsArts")
    ? JSON.parse(localStorage.getItem("cartItemsArts"))
    : [];
    setCartItemsArts(cartItemsArts)
  }, [])

  const changeFavItemsHandler = (itemArticul) => {
    const isFav = favItemsArts.includes(itemArticul);
    const newFavItemsArts = isFav
    ? favItemsArts.filter(articul => articul !== itemArticul)
    : [...favItemsArts , itemArticul];
    localStorage.setItem("favItemsArts", JSON.stringify(newFavItemsArts));    
    setFavItemsArts(newFavItemsArts)
  }

  const addToCartArtHandler = (itemArticul) => {
    setAddToCartArt(itemArticul);    
  }

  const changeCartItemsHandler = () => {
    const isInCart = cartItemsArts.includes(cartItemsArts);
    const newCartItemsArts = isInCart
    ? cartItemsArts.filter(articul => articul !== addToCartArt)
    : [...cartItemsArts , addToCartArt];
    localStorage.setItem("cartItemsArts", JSON.stringify(newCartItemsArts));    
    setCartItemsArts(newCartItemsArts)
  }

  const deleteFromCartArtHandler = (itemArticul) => {
    console.log("deleting", itemArticul)
    setDeleteFromCartArt(itemArticul);
  }

  const deleteItemFromCart = () => {
    console.log("cartItemsArts", cartItemsArts)
    const newCartItemsArts = cartItemsArts.filter(articul => (articul !== deleteFromCartArt));
    localStorage.setItem("cartItemsArts", JSON.stringify(newCartItemsArts));
    console.log("newCartItemsArts", newCartItemsArts)
    setCartItemsArts(newCartItemsArts);
  }

  const removeOneCartItem = (itemArticul) => {
    const newCartItemsArts = [
      ...cartItemsArts.slice(0, cartItemsArts.indexOf(itemArticul)),
      ...cartItemsArts.slice(cartItemsArts.indexOf(itemArticul)+1)
    ];
    localStorage.setItem("cartItemsArts", JSON.stringify(newCartItemsArts));
    setCartItemsArts(newCartItemsArts);
  }

  const addOneCartItem = (itemArticul) => {
    const newCartItemsArts = [...cartItemsArts, itemArticul];
    localStorage.setItem("cartItemsArts", JSON.stringify(newCartItemsArts));
    setCartItemsArts(newCartItemsArts);
  }

  return (<>
    <BrowserRouter>
      <Header />
      <Switch>
        <Route exact path="/" render={() => (
          <MainPage
            isLoadingSuccess={isLoadingSuccess}
            items={items}
            itemsListProps={{
              items, 
              cartItemsArts,
              favItemsArts,
              cardProps: {
                setAddtionModalState,
                changeFavItemsHandler,
                addToCartArtHandler
              },
              isAdditionModalOpen: isAdditionModalOpen,
              modalProps: {
                setAddtionModalState,
                onApprove: changeCartItemsHandler,
                text: "Add item to cart?"
              }
            }}
          />
        )}/>
        <Route exact path="/favourites" render={() => {
          if(items) {
            const favItems = items.filter(item => favItemsArts.includes(item.articul))
            return <FavouritesPage 
              isLoadingSuccess={isLoadingSuccess}
              items={favItems}
              itemsListProps={{
                items: favItems, 
                cartItemsArts,
                favItemsArts,
                cardProps: {
                  setAddtionModalState,
                  changeFavItemsHandler,
                  addToCartArtHandler
                },
                isAdditionModalOpen: isAdditionModalOpen,
                modalProps: {
                  setAddtionModalState,
                  onApprove: changeCartItemsHandler,
                  text: "Add item to cart?"
                }
              }}
            />
          }
        }} />
        <Route exact path="/cart" render={() => {
          if(items) {
            const cartItems = items.filter(item => cartItemsArts.includes(item.articul))
            console.log("cart items: ", cartItems, new Date())
            return <CartPage 
            isLoadingSuccess={isLoadingSuccess}
              items={cartItems}
              itemsListProps={{
                items: cartItems, 
                cartItemsArts,
                favItemsArts,
                cardProps: {
                  setAddtionModalState,
                  changeFavItemsHandler,
                  deleteFromCartArtHandler,
                  removeOneCartItem,
                  addOneCartItem,
                },
                isAdditionModalOpen: isAdditionModalOpen,
                modalProps: {
                  setAddtionModalState,
                  onApprove: deleteItemFromCart,
                  text: "Delete item from cart?"
                }
              }}
            />
          }
        }} />
      </Switch>
    </BrowserRouter>
  </>)

}

export default App
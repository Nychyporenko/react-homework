import React from "react";
import { CartItemCard } from "../../components/CartItemCard/CartItemCard.component";
import { ItemsList } from "../../components/ItemsList/ItemsList.component";

const CartPage = (props) => {

  const {
    isLoadingSuccess,
    items, 
    itemsListProps,
  } = props

  return (<>
    {
      !Array.isArray(items) &&
      <div>No items found</div>
    }
    <ItemsList  
      isLoadingSuccess={isLoadingSuccess}       
      items={items}
      itemsListProps={itemsListProps}
      Card={CartItemCard}
    />
  </>)
}

export {CartPage};
import React from "react";
import { ItemCard } from "../../components/ItemCard/ItemCard.component";
import { ItemsList } from "../../components/ItemsList/ItemsList.component";

const FavouritesPage = (props) => {

  const {
    isLoadingSuccess,
    items, 
    itemsListProps,
  } = props

  return (<>
    {
      !Array.isArray(items) &&
      <div>No items found</div>
    }
    <ItemsList  
      isLoadingSuccess={isLoadingSuccess}         
      items={items}
      itemsListProps={itemsListProps}
      Card={ItemCard}
    />
  </>)
}

export {FavouritesPage};
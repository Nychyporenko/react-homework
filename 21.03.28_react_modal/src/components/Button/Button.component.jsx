import React from "react";
import styles from "./button.module.scss";

class Button extends React.Component {
    constructor(props) {
      super(props)    
    }
    render() {
      console.log(styles)
      const {text, onClick, backgroundColor} = this.props;
      return (
        <button
          className={styles["choose-modal-btn"]}
          onClick={onClick}
          style={{backgroundColor: `${backgroundColor}`}}
        >{text}</button>
      )
    }
  }

  export {Button};
import React from "react";
import { Button } from "../Button/Button.component";
import styles from "./modal.module.scss";

class Modal extends React.Component {
    constructor(props) {
      super(props)
    }
  
    render() {
        console.log(styles);
      const {text, header, actions, closeButton, modalCloseHandler} = this.props;
      return (
      <div
        className={styles["backdrop"]}      
        onClick={modalCloseHandler}
      >
        <div className={styles.modal}
            onClick={e => {e.stopPropagation()}}
        >        
          <div className={styles["modal-header"]}>
            {header}
            {closeButton && 
                <Button
                    className={styles.closeModalBtn}
                    onClick={modalCloseHandler}  
                    text="✕"  
                />
            }
          </div>
          <div className={styles["modal-body"]}>{text}</div>
          <div className={styles["modal-actions"]}>{actions}</div>
        </div>
      </div>)
    }
  }

  export {Modal}
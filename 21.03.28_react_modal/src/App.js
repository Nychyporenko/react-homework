import React from "react";
import { Button } from "./components/Button/Button.component";
import { Modal } from "./components/Modal/Modal.component";
import styles from "./App.module.scss";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      modalId: null,
    }
  }

  openModalButtonHandler = (modalId) => {
    if(modalId === 1) {
      this.setState(_ => {return {modalId: 1}})
    } else if(modalId === 2) {
      this.setState(_ => {return {modalId: 2}})
    }
  }

  modalCloseHandler = () => {
    this.setState({modalId: null})
  }

  render() {
    const {modalId} = this.state;
    const buttonsJSX = <>
      <Button text="ok" />
      <Button
        onClick={this.modalCloseHandler}
        text="cancel" 
      />
    </>
    return (
    <>
      {modalId === 1 &&
        <Modal
          header="Do you want to delete this file?"
          text={"Once you delete this file, it won't be possible to undo this action.\nAre you sure want to delete it?"} 
          closeButton={true}
          actions={buttonsJSX}
          modalCloseHandler={this.modalCloseHandler}
          modalId={this.state.modalId}
        />
      }
      {modalId === 2 &&
        <Modal
          header="another header"
          text="modal 2"
          closeButton={false}
          actions={buttonsJSX}
          modalCloseHandler={this.modalCloseHandler}
          modalId={this.state.modalId}
        />
      }
      {!modalId &&
        <div className={styles["choose-modal"]}>
          <Button        
            text="Open first modal" 
            onClick={() => {this.openModalButtonHandler(1)}}
            backgroundColor="lightsalmon"
          />
          <Button 
            text="Open second modal" 
            onClick={() => {this.openModalButtonHandler(2)}}
            backgroundColor="lightgreen"
          />
        </div>
      }
    </>
    )
  }
}

export default App